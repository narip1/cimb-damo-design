const {apiUrl} = process.env

const state = () => ({
  loading: false
})

const mutations = {
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  }
}

const actions = {
  loadAddress({ commit, rootState }){
    commit('setLoading')
    return this.$axios.get(`${apiUrl}/me/${rootState.user.userProfile.id}/addresses`,{
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },
  loadAddressDetail({ commit, rootState }, address_id){
    commit('setLoading')
    return this.$axios.get(`${apiUrl}/me/${rootState.user.userProfile.id}/addresses/${address_id}`,{
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },
  addAddress({ commit, rootState }, address){
    commit('setLoading')
    return this.$axios.post(`${apiUrl}/me/${rootState.user.userProfile.id}/addresses`, address,{
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },
  updateAddress({ commit, rootState }, address){
    commit('setLoading')
    return this.$axios.patch(`${apiUrl}/me/${rootState.user.userProfile.id}/addresses/${address.id}`, {
      building: address.building,
      address: address.address,
      street: address.street,
      province: address.province,
      district: address.district,
      sub_district: address.sub_district,
      postcode: address.postcode,
      tel: address.tel
    },{
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },
  deleteAddress({ commit, rootState }, address_id){
    commit('setLoading')
    return this.$axios.delete(`${apiUrl}/me/${rootState.user.userProfile.id}/addresses/${address_id}`,{
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
