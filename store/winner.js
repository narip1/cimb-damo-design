const {apiUrl} = process.env

const state = () => ({
  loading: false
})

const mutations = {
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  }
}

const actions = {
  getWinners({ commit, rootState }){
    commit('setLoading')

    return this.$axios.get(`${apiUrl}/winners`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
