const {apiUrl} = process.env

const state = () => ({
  loading: false
})

const mutations = {
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  }
}

const actions = {
  loadHistoryIncrement({ commit, rootState }, payload){
    commit('setLoading')
    return this.$axios.get(`${apiUrl}/histories?increment=true&month=${payload.month}&year=${payload.year}`,{
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },
  loadHistoryDecrement({ commit, rootState }, payload){
    commit('setLoading')
    return this.$axios.get(`${apiUrl}/histories?decrement=true&month=${payload.month}&year=${payload.year}`,{
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },
  loadMore({ commit, rootState }, url){
    commit('setLoading')
    return this.$axios.get(`${url}`,{
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },
  loadHistoryDetail({ commit, rootState }, history_id){
    commit('setLoading')
    return this.$axios.get(`${apiUrl}/histories/${history_id}`,{
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data.data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
