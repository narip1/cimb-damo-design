const {apiUrl} = process.env

const state = () => ({
  privilegeList: [],
  categories: [],
  loading: false,
  categoryLoading: false,
  privilegeDetailLoading: false,
  privilegeLoadMore: false,
  privilegeRedeemLoading: false,
  loaded: false,
  category_id: null
})

const mutations = {
  setPrivilegeList(state, privilegeList = []) {
    let categories = _.map(state.categories, (category) => {
      if (category.category_id === state.category_id) {
        category.privilegeList = privilegeList
      }
      return category
    })

    state.categories = categories
  },
  setPage(state, page = null) {
    let categories = _.map(state.categories, (category) => {
      if (category.category_id === state.category_id) {
        category.page = page
      }
      return category
    })

    state.categories = categories
  },
  setPrivilegeMoreList(state, privilegeList = []) {
    let categories = _.map(state.categories, (category) => {
      if (category.category_id === state.category_id) {
        let privilege_list_data = category.privilegeList

        _.each(privilegeList, (privilege)=>{
          privilege_list_data.push(privilege)
        })

        category.privilegeList = privilege_list_data
      }
      return category
    })

    state.categories = categories
  },
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  },
  setPrivilegeDetailLoading(state, isLoading = true) {
    state.privilegeDetailLoading = isLoading
  },
  setPrivilegeLoadMoreLoading(state, isLoading = true) {
    state.privilegeLoadMore = isLoading
  },
  setPrivilegeRedeemLoading(state, isLoading = true) {
    state.privilegeRedeemLoading = isLoading
  },
  setCategoryLoading(state, isLoading = true) {
    state.categoryLoading = isLoading
  },
  setLoaded(state, isLoaded = true) {
    state.loaded = isLoaded
  },
  setCategory(state, categories = []) {
    categories = _.map(categories, (category) => {
      category.privilegeList = []
      category.page = {
        current_page: 1,
        per_page: 4,
        total_item: 0,
        total_page: 1
      }
      return category
    })

    state.categories = categories
  },
  setCategoryId(state, category_id = null) {
    state.category_id = category_id
  }
}

const actions = {
  getCategories({commit, state, rootState}) {
    commit('setCategoryLoading')

    if (state.loaded) {
      commit('setCategoryLoading', false)
      return new Promise((resolve) => {
        resolve({
          PrivilegeCategory: state.categories
        })
      })
    }

    return this.$axios.get(`${apiUrl}/privileges/categories`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      commit('setCategory', data.PrivilegeCategory)
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    })
      .catch((error) => {
        // alert(error)
        commit('setCategory')
        commit('setLoaded', false)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setCategoryLoading', false)
      })
  },

  getPrivilege({state, commit, rootState}, payload) {
    commit('setLoading')

    let category = _.find(state.categories, (category) => {
      return category.category_id === payload.category_id
    })

    if(category.privilegeList.length === 0){
      return this.$axios.get(`${apiUrl}/privileges?category_id=${payload.category_id}&page=${payload.current_page}`, {
        headers: {
          'Accept': 'application/json',
          'Authorization': `Bearer ${rootState.user.token}`
        }
      })
        .then(({data}) => {
          commit('setCategoryId', payload.category_id)
          commit('setPrivilegeList', data.privilege)
          commit('setPage', data.page)
          return new Promise((resolve) => {
            resolve(data.privilege)
          })
        })
        .catch((error) => {
          // alert(error)
          return new Promise((resolve, reject) => {
            reject(error)
          })
        })
        .finally(() => {
          commit('setLoading', false)
        })
    }else{
      commit('setLoading', false)
      return new Promise((resolve) => {
        resolve(category.privilegeList)
      })
    }
  },

  getPrivilegeMore({commit, rootState}, payload) {
    commit('setLoading')
    commit('setPrivilegeLoadMoreLoading')
    return this.$axios.get(`${apiUrl}/privileges?category_id=${payload.category_id}&page=${payload.current_page}`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        commit('setCategoryId', payload.category_id)
        commit('setPrivilegeMoreList', data.privilege)
        commit('setPage', data.page)
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
        commit('setPrivilegeLoadMoreLoading', false)
      })
  },

  addFavorite({commit, rootState}, privilege_id) {
    commit('setLoading')

    let latitude = 0
    let longitude = 0

    if (rootState.user.location) {
      latitude = rootState.user.location.latitude
      longitude = rootState.user.location.longitude
    }

    return this.$axios.post(`${apiUrl}/me/${rootState.user.userProfile.id}/favorites`, {
      privilege_id: privilege_id
    }, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        // alert(error)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  getPrivilegeDetail({commit, rootState}, privilege_id) {
    commit('setPrivilegeDetailLoading')
    return this.$axios.get(`${apiUrl}/privileges/${privilege_id}/detail`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        // alert(error)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setPrivilegeDetailLoading', false)
      })
  },

  getPrivilegeRedeem({commit, rootState}, privilege_code) {
    commit('setPrivilegeRedeemLoading')
    return this.$axios.post(`${apiUrl}/privileges/${privilege_code}/redeem`, {}, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      return new Promise((resolve) => {
        resolve(data)
      })
    }).catch((error) => {
      // alert(error)
      return new Promise((resolve, reject) => {
        reject(error)
      })
    })
      .finally(() => {
        commit('setPrivilegeRedeemLoading', false)
      })
  },

  getPrivilegeRedeemAddress({commit, rootState}, payload) {
    commit('setPrivilegeRedeemLoading')
    return this.$axios.post(`${apiUrl}/privileges/${payload.privilege_code}/redeem`, {
      address_id: payload.address_id
    }, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      return new Promise((resolve) => {
        resolve(data)
      })
    }).catch((error) => {
      // alert(error)
      return new Promise((resolve, reject) => {
        reject(error)
      })
    })
      .finally(() => {
        commit('setPrivilegeRedeemLoading', false)
      })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
