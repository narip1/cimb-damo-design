const {apiUrl} = process.env

const state = () => ({
  favoriteList: [],
  loading: false
})

const mutations = {
  setFavoriteList(state, favoriteList = []) {
    state.favoriteList = favoriteList
  },
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  }
}

const actions = {
  getFavorite({ commit, rootState }, payload){
    commit('setLoading')
    // return this.$axios.get(`${apiUrl}/me/${rootState.user.userProfile.id}/favorites&page=${payload.current_page}`, {
    return this.$axios.get(`${apiUrl}/me/${rootState.user.userProfile.id}/favorites`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        commit('setFavoriteList', data)
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        commit('setFavoriteList')
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })

    // commit('setFavoriteList', [
    //   {
    //     id: 1,
    //     is_favorite: true,
    //     require_point: 25,
    //     caption: "โดนัทชิ้นที่ 2 (ในราคาที่เท่ากันหรือต่ำกว่า) เมื่อซื้อโดนัทชิ้นแรกในราคา 25 บาท",
    //     remaining: 82,
    //     image: require('../assets/image-privilege/privilege1.png')
    //   },
    //   {
    //     id: 2,
    //     is_favorite: true,
    //     require_point: 49,
    //     caption: "Start You Party ปาร์ตี้ปีใหม่ เปิดความสุขถึงที่ กับเคเอฟซี ชุกปาร์ตี้ีใหม่",
    //     remaining: 38,
    //     image: require('../assets/image-privilege/privilege2.png')
    //   },
    //   {
    //     id: 3,
    //     is_favorite: true,
    //     require_point: 49,
    //     caption: "รับส่วนลด 50.- เมื่อซื้ออาหาร และเครื่องดื่มครบ 400.- ตึ่งแต่วันนี้",
    //     remaining: 233,
    //     image: require('../assets/image-privilege/privilege3.png')
    //   }
    // ])
  },
  // addFavorite({ commit, rootState }, privilege_id){
  //   // commit('setLoading')
  //
  //   let latitude = 0
  //   let longitude = 0
  //
  //   if(rootState.user.location){
  //     latitude = rootState.user.location.latitude
  //     longitude = rootState.user.location.longitude
  //   }
  //
  //   return this.$axios.post(`${apiUrl}/me/${rootState.user.userProfile.id}/favorites`, {
  //     privilege_id: privilege_id
  //   },{
  //     headers: {
  //       'Accept': 'application/json',
  //       'Authorization': `Bearer ${rootState.user.token}`
  //     }
  //   })
  //     .then(({data}) => {
  //       return new Promise((resolve) => {
  //         resolve(data)
  //       })
  //     })
  //     .catch((error) => {
  //       // alert(error)
  //       return new Promise((resolve, reject) => {
  //         reject(error)
  //       })
  //     })
  //     .finally(() => {
  //       commit('setLoading', false)
  //     })
  // }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
