const {apiUrl} = process.env

const state = () => ({
  userProfile: null,
  token: 'jOA0G4i0zvdgMA4VihDi',
  loading: false,
  loaded: false,
  location: null,
  line_token:'eyJhbGciOiJIUzI1NiJ9.pFUaSiMY_AGOMSIcQZFPGStoLEvEGiFINCgy1peLdK9iExJaFqVBWbA6vk-0CHJUHqjApaI2gv24E4tfdpe9iUZ-tAkzZ-7zVD7upTCmSgR6oWCAovl_3NmK1j8vV_AFu0Hr9tg-F3kqk1Aid50RXFUQ1kEr_3kOw9rjYDTpzng.LdEnQ2Bilap3Zd_17Nghsd0WrHGVCcdQm_tVbG_tm1c'})

const mutations = {
  setUser(state, user = null) {
    state.userProfile = user
  },
  setToken(state, token = null) {
    state.token = token
  },
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  },
  setLoaded(state, isLoaded = true) {
    state.loaded = isLoaded
  },
  setLocation(state, location = null){
    state.location = location
  },
  setLineToken(state, line_token = null) {
    state.line_token = line_token
  },
}

const actions = {
  // checkLineId({commit}, lineId){
  checkLineId({commit}, token){
    commit('setLoading')
    return this.$axios.post(`${apiUrl}/check`, {
      // line_id: lineId
      token: token
    },{
      headers: {
        'Accept': 'application/json'
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        // alert(error)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  setOtp({ commit }, mobile){
    commit('setLoading')
    return this.$axios.post(`${apiUrl}/send-otp`, {
      phone_number: mobile
    },{
      headers: {
        'Accept': 'application/json'
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  verifyMobile({ commit }, payload){
    commit('setLoading')
    return this.$axios.post(`${apiUrl}/verify`, {
      phone_number: payload.mobile,
      code: payload.otp,
      reference: payload.ref
    },{
      headers: {
        'Accept': 'application/json'
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  register({ commit }, payload){
    commit('setLoading')
    return this.$axios.post(`${apiUrl}/register`, payload,{
      headers: {
        'Accept': 'application/json'
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  setToken({ commit }, token){
    commit('setToken', token)
    localStorage.setItem('token', token)
  },

  getProfile({commit, state}){
    commit('setLoading')

    if (!state.token) {
      if (localStorage.token) {
        commit('setToken', localStorage.token)
      }
    }

    return this.$axios.get(`${apiUrl}/me`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${state.token}`
      }
    })
      .then(({data}) => {
        commit('setUser', data.data)
        commit('setLoaded')
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        localStorage.removeItem('token')
        commit('setToken')
        commit('setUser')
        commit('setLoaded', false)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  getProfileWithOutLoaded({commit, state}){
    commit('setLoading')

    if (!state.token) {
      if (localStorage.token) {
        commit('setToken', localStorage.token)
      }
    }

    if(state.loaded){
      commit('setLoading', false)
      return new Promise((resolve) => {
        resolve({
          data: state.userProfile
        })
      })
    }

    return this.$axios.get(`${apiUrl}/me`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${state.token}`
      }
    })
      .then(({data}) => {
        commit('setUser', data.data)
        commit('setLoaded')
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        localStorage.removeItem('token')
        commit('setToken')
        commit('setUser')
        commit('setLoaded', false)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  updateProfile({commit, state}, profile){
    commit('setLoading')
    return this.$axios.patch(`${apiUrl}/me/${profile.user_id}`, {
      line_display_name: profile.line_display_name,
      line_picture_url: profile.line_picture_url,
      line_status_message: profile.line_status_message,
      user_id: profile.user_id,
      title_name: profile.title_name,
      first_name: profile.first_name,
      last_name: profile.last_name,
      mobile: profile.mobile,
      email: profile.email,
      birth_date: profile.birth_date,
      province: profile.province,
      district: profile.district,
      // sub_district: profile.sub_district,
      // address: profile.address,
      // zip_code: profile.zip_code,
    }, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${state.token}`
      }
    })
      .then(({data}) => {
        commit('setUser', data.data)
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },

  setLocation({commit, state}, location){
    commit('setLocation', location)
  },

  setLineToken({commit, state}, token){
    commit('setLineToken', token)
    localStorage.setItem('line-token', token)
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
