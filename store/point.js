const {apiUrl} = process.env

const state = () => ({
  loading: false
})

const mutations = {
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  }
}

const actions = {
  addPoint({ commit, rootState }, code){
    commit('setLoading')

    let latitude = 0
    let longitude = 0

    if(rootState.user.location){
      latitude = rootState.user.location.latitude
      longitude = rootState.user.location.longitude
    }

    return this.$axios.post(`${apiUrl}/points`, {
      code: code,
      latitude: latitude,
      longitude: longitude
    },{
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        // alert(error)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },
  callOcr({ commit, rootState }, file){
    let formData = new FormData()
    formData.append('file', file)

    commit('setLoading')
    return this.$axios.post(`${apiUrl}/detectors`, formData,{
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    })
      .then(({data}) => {
        return new Promise((resolve) => {
          resolve(data)
        })
      })
      .catch((error) => {
        // alert(error)
        return new Promise((resolve, reject) => {
          reject(error)
        })
      })
      .finally(() => {
        commit('setLoading', false)
      })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
