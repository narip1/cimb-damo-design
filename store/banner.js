const {apiUrl} = process.env

const state = {
  loading: false,
  loaded: false,
  banners: [],
}

const actions = {
  getBanner({ state, commit, rootState}) {
    commit('setLoading')

    if(state.loaded){
      commit('setLoading', false)
      return new Promise((resolve) => {
        resolve(state.banners)
      })
    }

    return this.$axios.$get(`${apiUrl}/banners`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${rootState.user.token}`
      }
    }).then(({data}) => {
      commit('setBanner', data)
      commit('setLoaded')
      return new Promise((resolve) => {
        resolve(data)
      })
    }).catch((error) => {
      commit('setBanner')
      commit('setLoaded', false)
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }).finally(() => {
      commit('setLoading', false)
    })
  },
}

const mutations = {
  setLoading(state, isLoading = true) {
    state.loading = isLoading
  },

  setLoaded(state, isLoaded = true) {
    state.loaded = isLoaded
  },

  setBanner(state, banner = []) {
    state.banners = banner
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
