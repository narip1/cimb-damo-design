import Vue from 'vue'
const moment = require('moment')

Vue.prototype.$liff = window.liff

Vue.mixin({
  methods:{
    handleCatch(error){
      if (error.response) {
        let title = ''
        let message = ''

        if(error.response.status === 401){
          this.$router.replace('/')
          return
        }

        if(error.response.data){
          if(error.response.data.messages){
            message = error.response.data.messages.error
          }else if(error.response.data.message){
            message = error.response.data.message
          }else{
            title = error.response.statusText + ' ('+error.response.status+')'
            message = error.response.data.exception
          }
        }else{
          title = error.response.statusText + ' ('+error.response.status+')'
          message = error.response.data.exception
        }

        $.alert({
          title: title,
          content: message
        })
      }
    },
    // lineOAuth(){
    //   let client_id = '1653710311';
    //   let redirect_uri = 'https://d-double-a.feyverly-crm.com/get-point/';
    //   let state = 'login';
    //   let response_type = 'code';
    //   let scope = 'openid%20profile';
    //
    //   if (this.$route.query.token) {
    //     if (this.$route.query.type === 'android') {
    //       this.lineProfile(this.$route.query.token)
    //     } else {
    //       if (this.$route.query.code) {
    //         this.lineAccessToken()
    //       } else {
    //         window.location.href = `https://access.line.me/oauth2/v2.1/authorize?response_type=${response_type}&client_id=${client_id}&redirect_uri=${redirect_uri}&state=${state}&scope=${scope}`
    //       }
    //     }
    //   } else {
    //     if (this.$route.query.code) {
    //       this.lineAccessToken()
    //     } else {
    //       window.location.href = `https://access.line.me/oauth2/v2.1/authorize?response_type=${response_type}&client_id=${client_id}&redirect_uri=${redirect_uri}&state=${state}&scope=${scope}`
    //     }
    //   }
    // },
    // lineAccessToken() {
    //   let redirect_uri = 'https://d-double-a.feyverly-crm.com';
    //   let body = new URLSearchParams;
    //   body.append('grant_type', 'authorization_code')
    //   body.append('code', this.$route.query.code)
    //   body.append('redirect_uri', redirect_uri)
    //   body.append('client_id', '1653710311')
    //   body.append('client_secret', '51c2494749b8652c55c79b261fc2bc65')
    //   this.$axios.post('https://api.line.me/oauth2/v2.1/token', body, {
    //     headers: {
    //       'Content-Type': 'application/x-www-form-urlencoded'
    //     }
    //   }).then((data) => {
    //     let userAgent = navigator.userAgent
    //
    //     if (userAgent.search('Android') >= 0 || userAgent.search('android') >= 0) {
    //       window.location.href = `intent://d-double-a.feyverly-crm.com?type=android&token=${data.data.access_token}#Intent;scheme=http;package=com.android.chrome;end`
    //       // window.location.href = `https://d-double-a.feyverly-crm.com?type=android&token=${data.data.access_token}`
    //     } else {
    //       this.getLineProfile(data.data.access_token)
    //     }
    //   })
    // },
    // lineProfile(token) {
    //   this.$store.dispatch('user/setLineToken', token)
    //
    //   this.$axios.get('https://api.line.me/v2/profile', {
    //     headers: {
    //       'Authorization': `Bearer ${token}`
    //     }
    //   }).then((lineProfile) => {
    //     this.$store.dispatch('user/checkLineId', lineProfile.data.userId)
    //       .then((data) => {
    //         if (data.data) {
    //           // is member
    //           this.$store.dispatch('user/setToken', data.token)
    //
    //           this.$router.replace('/privilege')
    //         } else {
    //           // go register
    //           this.$router.replace('/register')
    //         }
    //
    //       })
    //       .catch((error) => {
    //         this.handleCatch(error)
    //       })
    //   })
    // },
    handleCatchAndLoginLine(error, query = null){
      if (error.response) {
        let title = ''
        let message = ''

        if(error.response.status === 401){
          if(query){
            this.$router.replace(`/?page=${query}`)
          }else{
            this.$router.replace('/')
          }

          // $.confirm({
          //   title: '',
          //   content: 'คุณยังไม่ได้ทำการลงทะเบียน กรุณาลงทะเบียนก่อน',
          //   buttons:{
          //     register: {
          //       btnClass: 'btn-outline-dark',
          //       text: 'ลงทะเบียน',
          //       action: ()=>{
          //         this.$router.replace('/')
          //       }
          //     }
          //   }
          // })
        }else{
          if(error.response.data){
            if(error.response.data.messages){
              message = error.response.data.messages.error
            }else if(error.response.data.message){
              message = error.response.data.message
            }else{
              title = error.response.statusText + ' ('+error.response.status+')'
              message = error.response.data.exception
            }
          }else{
            title = error.response.statusText + ' ('+error.response.status+')'
            message = error.response.data.exception
          }

          $.alert({
            title: title,
            content: message
          })
        }
      }
    },
    handleCatchAndRedirectToLoginLine(error){
      if (error.response) {
        let title = ''
        let message = ''

        if(error.response.status === 401){
          this.$router.replace('/?page=get-point')
          // window.location.href = `intent://d-double-a.feyverly-crm.com?page=get-point#Intent;scheme=http;package=com.android.chrome;end`
          // $.confirm({
          //   title: '',
          //   content: 'คุณยังไม่ได้ทำการลงทะเบียน กรุณาลงทะเบียนก่อน',
          //   buttons:{
          //     register: {
          //       btnClass: 'btn-outline-dark',
          //       text: 'ลงทะเบียน',
          //       action: ()=>{
          //         // let client_id = '1653758497';
          //         // let redirect_uri = 'https://d-double-a.feyverly-crm.com';
          //         // let state = 'login';
          //         // let response_type = 'code';
          //         // let scope = 'openid%20profile';
          //         // let url = `https%3A%2F%2Faccess.line.me%2Foauth2%2Fv2.1%2Fauthorize%3Fresponse_type%3D${response_type}%26client_id%3D${client_id}%26redirect_uri%3D${redirect_uri}%26state%3D${state}%26scope%3D${scope}`
          //         // window.location.href = `intent://${url}#Intent;scheme=http;package=com.android.chrome;end`
          //         window.location.href = `intent://d-double-a.feyverly-crm.com?page=get-point#Intent;scheme=http;package=com.android.chrome;end`
          //       }
          //     }
          //   }
          // })
        }else{
          if(error.response.data){
            if(error.response.data.messages){
              message = error.response.data.messages.error
            }else if(error.response.data.message){
              message = error.response.data.message
            }else{
              title = error.response.statusText + ' ('+error.response.status+')'
              message = error.response.data.exception
            }
          }else{
            title = error.response.statusText + ' ('+error.response.status+')'
            message = error.response.data.exception
          }

          $.alert({
            title: title,
            content: message
          })
        }
      }
    },
    formatPrice(value) {
      if(value){
        return parseFloat(value).toFixed(0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      }
      return 0
    },
    makeQueryString(data){
      let queryString = []

      _.each(data, (value, key) => {
        queryString.push(key + '=' + value)
      })

      queryString = _.join(queryString, '&')

      return queryString
    },
    transactionType(type, transactionType){
      if(transactionType === 'voucher'){
        return 'ใช้คูปอง'
      }
      if(transactionType === 'point'){
        if(type === '0'){
          return 'ใช้คะแนน'
        }else{
          return 'รับคะแนน'
        }
      }
      if(transactionType === 'credit'){
        if(type === '0'){
          return 'ใช้เครดิต'
        }else{
          return 'เติมเครดิต'
        }
      }
    }
  },
  filters: {
    formatMobileCode(value){
      if(value.substring(0,1) === "0"){
        return "66"+value.substring(1)
      }
      return value
    },
    moment(value, format) {
      if(value){
        return moment(value).format(format)
      }
      return ''
    },
    limitString(value, max = 200, format = ' ...') {
      if (value !== null || value !== undefined) {
        if (value.length > max) {
          return value.substr(0, max - 1) + format
        }
      }
      return value
    },
    formatPrice(value) {
      if(value){
        return parseFloat(value).toFixed(0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      }
      return 0
    },
    formatPriceAndTextVoucher(value) {
      if(value){
        if(value === 'voucher'){
          return 'voucher'
        }
        return parseFloat(value).toFixed(0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      }
      return 0
    }
  }
})
