import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _29400cc8 = () => interopDefault(import('../pages/address/index.vue' /* webpackChunkName: "pages/address/index" */))
const _7ef7c798 = () => interopDefault(import('../pages/check-line.vue' /* webpackChunkName: "pages/check-line" */))
const _82956d28 = () => interopDefault(import('../pages/condition.vue' /* webpackChunkName: "pages/condition" */))
const _6e5a6bc4 = () => interopDefault(import('../pages/favorite/index.vue' /* webpackChunkName: "pages/favorite/index" */))
const _327eadaa = () => interopDefault(import('../pages/get-point.vue' /* webpackChunkName: "pages/get-point" */))
const _3bb452b6 = () => interopDefault(import('../pages/history.vue' /* webpackChunkName: "pages/history" */))
const _e37a79d2 = () => interopDefault(import('../pages/me.vue' /* webpackChunkName: "pages/me" */))
const _0b74b1da = () => interopDefault(import('../pages/ModalHistoryCode.vue' /* webpackChunkName: "pages/ModalHistoryCode" */))
const _6673ba8c = () => interopDefault(import('../pages/popup_use-rg-pl.vue' /* webpackChunkName: "pages/popup_use-rg-pl" */))
const _16f9d5f6 = () => interopDefault(import('../pages/privilege/index.vue' /* webpackChunkName: "pages/privilege/index" */))
const _507c2d8c = () => interopDefault(import('../pages/profile.vue' /* webpackChunkName: "pages/profile" */))
const _4235c786 = () => interopDefault(import('../pages/profile/index.vue' /* webpackChunkName: "pages/profile/index" */))
const _745e5d62 = () => interopDefault(import('../pages/register.vue' /* webpackChunkName: "pages/register" */))
const _34e15110 = () => interopDefault(import('../pages/successaddress.vue' /* webpackChunkName: "pages/successaddress" */))
const _6d7aa130 = () => interopDefault(import('../pages/address/_id.vue' /* webpackChunkName: "pages/address/_id" */))
const _0028bc06 = () => interopDefault(import('../pages/favorite/_id.vue' /* webpackChunkName: "pages/favorite/_id" */))
const _6d6c94ad = () => interopDefault(import('../pages/privilege/_id.vue' /* webpackChunkName: "pages/privilege/_id" */))
const _6ca6d623 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/address",
    component: _29400cc8,
    name: "address"
  }, {
    path: "/check-line",
    component: _7ef7c798,
    name: "check-line"
  }, {
    path: "/condition",
    component: _82956d28,
    name: "condition"
  }, {
    path: "/favorite",
    component: _6e5a6bc4,
    name: "favorite"
  }, {
    path: "/get-point",
    component: _327eadaa,
    name: "get-point"
  }, {
    path: "/history",
    component: _3bb452b6,
    name: "history"
  }, {
    path: "/me",
    component: _e37a79d2,
    name: "me"
  }, {
    path: "/ModalHistoryCode",
    component: _0b74b1da,
    name: "ModalHistoryCode"
  }, {
    path: "/popup_use-rg-pl",
    component: _6673ba8c,
    name: "popup_use-rg-pl"
  }, {
    path: "/privilege",
    component: _16f9d5f6,
    name: "privilege"
  }, {
    path: "/profile",
    component: _507c2d8c,
    children: [{
      path: "",
      component: _4235c786,
      name: "profile"
    }]
  }, {
    path: "/register",
    component: _745e5d62,
    name: "register"
  }, {
    path: "/successaddress",
    component: _34e15110,
    name: "successaddress"
  }, {
    path: "/address/:id",
    component: _6d7aa130,
    name: "address-id"
  }, {
    path: "/favorite/:id",
    component: _0028bc06,
    name: "favorite-id"
  }, {
    path: "/privilege/:id",
    component: _6d6c94ad,
    name: "privilege-id"
  }, {
    path: "/",
    component: _6ca6d623,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
